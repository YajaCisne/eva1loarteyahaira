package edu.ec.istdab.service;

import edu.ec.istdab.model.Usuario;

public interface IUsuarioService {
	Usuario login(Usuario us);

}
