package edu.ec.istdab.service.impl;

import java.io.Serializable;

import javax.ejb.EJB;

import org.mindrot.jbcrypt.BCrypt;

import edu.ec.istdab.dao.IUsuarioDAO;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IUsuarioService;

public class UsuarioServiceImpl implements IUsuarioService, Serializable{
	@EJB
	private IUsuarioDAO dao;

	@Override
	public Usuario login(Usuario us) {
		Usuario usuario = null;
		String clave = us.getClave();
		String claveHash = dao.traerPassHasched(us.getUsuario());
		try {
			if (!claveHash.isEmpty()) {
				if (BCrypt.checkpw(clave, claveHash)) {
					return dao.leerPorNombreUsuario(us.getUsuario());
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
		return null;
	}
	
	
	

}
