package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IPersonaService;

@Named
@ViewScoped
public class PersonaBean implements Serializable{
	@Inject
	private IPersonaService service;
	private Persona persona;
	private List<Persona> lista;
	private Usuario us;
	
	private String tipoDialogo;

	public List<Persona> getLista() {
		return lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	@PostConstruct
	public void init() {
		this.persona = new Persona();
		
		FacesContext contex = FacesContext.getCurrentInstance();
		this.us = (Usuario) contex.getExternalContext().getSessionMap().get("usuario");
		
		this.listar();
	}
	// metodos
		public void operar(String accion) {
			try {
				if (accion.equalsIgnoreCase("R")) {
					this.service.registrar(persona);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Persona Registrada",
							"Bienvenido"));
				} else if (accion.equalsIgnoreCase("M")) {
					this.service.modificar(persona);
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Operación Exitosa", 
							"Persona Modificada"));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void handleFileUpload(FileUploadEvent event) {
			try {
				if (event.getFile() != null) {
					this.persona.setFoto(event.getFile().getContents());
				}
			} catch (Exception e) {

			}
		}

		public void mostrarData(Persona p) {
			this.persona = p;
			this.tipoDialogo = "Modificar Persona";
		}

		public void listar() {
			try {
				this.lista = this.service.listar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void limpiarControles() {
			this.persona = new Persona();
			this.tipoDialogo = "Nueva Persona";
		}

		public String getTipoDialogo() {
			return tipoDialogo;
		}

		public void setTipoDialogo(String tipoDialogo) {
			this.tipoDialogo = tipoDialogo;
		}
		

	

}
